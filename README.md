# Taller Práctico: Implementación de Colas con AWS SQS, SNS y S3

## Introducción

En este taller, exploraremos la implementación de colas utilizando AWS Simple Queue Service (SQS) y Simple Storage Service (S3) de Amazon Web Services. Este taller tiene como objetivo proporcionar una comprensión práctica de cómo las colas se utilizan en microservicios y por qué son esenciales para sistemas empresariales y bancarios de gran escala.

## Concepto de Colas

Las colas son una parte fundamental de la arquitectura de sistemas distribuidos. Permiten la comunicación asincrónica entre diferentes componentes de una aplicación, lo que mejora la escalabilidad, la confiabilidad y la resistencia de los sistemas. Las colas ayudan a desacoplar los productores de eventos de los consumidores, lo que significa que los consumidores pueden procesar eventos en su propio ritmo. Esto es crucial para evitar la sobrecarga de recursos y garantizar un rendimiento constante en sistemas con altas cargas de trabajo.

## Tecnologías Utilizadas

### AWS SQS (Simple Queue Service)

Amazon SQS es un servicio de mensajería totalmente administrado que permite la comunicación entre diferentes componentes de una aplicación. Proporciona colas que actúan como intermediarios entre productores y consumidores, garantizando la entrega confiable de mensajes.

### AWS S3 (Simple Storage Service)

Amazon S3 es un servicio de almacenamiento en la nube altamente escalable y duradero. En este taller, utilizaremos S3 para simular la carga y el almacenamiento de archivos que se procesarán a través de nuestras colas.

## Arquitectura de Referencia

![Arquitectura de Referencia](![Alt text](KLTF.drawio-1.png))

La arquitectura de referencia consta de los siguientes componentes:

1. **Microservicio**: Este microservicio implementará tanto la funcionalidad de consumidor. Tendrá el siguiente endpoint:
   - `/consumir`: Este endpoint permite al microservicio actuar como consumidor, procesando los mensajes de la cola SQS y realizando acciones basadas en los archivos de S3.
   
2. **Cola SQS**: La cola de mensajes de SQS donde se almacenan los mensajes enviados por el microservicio productor.

3. **SNS Topic**: Un tema de SNS que notifica al microservicio cuando hay nuevos mensajes en la cola SQS (no se implementara).

4. **Bucket S3**: El bucket de S3 donde se cargan los archivos que deben ser procesados.

## Requerimientos (Tareas del Taller)

### 0. Microservicio - Servicio
- Debera tener desplegado en digital ocean el servicio base de calse 

### 1. Configuración de AWS (opcional)

- Cree una cuenta de AWS si aún no la tiene.
- Configure los permisos de IAM para que el microservicio pueda interactuar con SQS y S3.

### 2. Creación de Cola SQS (opcional)

- Cree una cola SQS.

### 3. Implementación del Microservicio

- Desarrolle el microservicio con los siguientes endpoints:
   - `/consumir`: Implemente la lógica para consumir mensajes de la cola SQS y procesar los archivos de S3.

### 4. Pruebas y Evaluación

- Se trabajara en clase para hacer las pruebas y competencia consumiento una cola previamente creada..

## Conclusiones

Las colas desempeñan un papel crítico en la construcción de sistemas distribuidos y de alta demanda. Al implementar colas como AWS SQS en su arquitectura, se logra una serie de beneficios clave:

- **Escalabilidad**: Las colas permiten que los sistemas se escalen de manera efectiva, ya que los productores pueden enviar mensajes a su propio ritmo, y los consumidores pueden procesarlos sin presión adicional.

- **Resistencia**: Almacenar mensajes en una cola proporciona una capa adicional de resistencia. Si un componente se cae o experimenta problemas, los mensajes todavía se mantienen en la cola y pueden ser procesados más tarde.

- **Desacoplamiento**: El uso de colas permite un alto grado de desacoplamiento entre los componentes de la aplicación. Esto facilita la integración de nuevos servicios y la evolución de la arquitectura.


Comprender y utilizar las colas, junto con servicios como S3, es esencial para diseñar sistemas empresariales robustos, escalables y confiables en la nube. La arquitectura de sistemas distribuidos modernos se basa en estos conceptos para enfrentar los desafíos de la alta demanda y la escalabilidad.

**Nota**: El servicio debe estar desplegado en Digital Ocean y debe tener su respectivo repositorio
