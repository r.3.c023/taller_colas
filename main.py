from random import randint
from typing import Union, List
from fastapi import FastAPI
import boto3
import uuid

queue_url = 'https://sqs.us-east-2.amazonaws.com/905418176240/tallerColas.fifo'
aws_access_key_id = "AKIA5FTZA63YJJO7YOF7"
aws_secret_access_key = "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"

sqs = boto3.client(
    'sqs',
    region_name='us-east-2',
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)

app = FastAPI()

class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None

class ArbolBinarioBusqueda:
    def __init__(self):
        self.raiz = None

    def insertar(self, valor):
        if self.raiz is None:
            self.raiz = Nodo(valor)
        else:
            self._insertar_recursivo(self.raiz, valor)

    def _insertar_recursivo(self, nodo, valor):
        if valor < nodo.valor:
            if nodo.izquierda is None:
                nodo.izquierda = Nodo(valor)
            else:
                self._insertar_recursivo(nodo.izquierda, valor)
        else:
            if nodo.derecha is None:
                nodo.derecha = Nodo(valor)
            else:
                self._insertar_recursivo(nodo.derecha, valor)

    def en_orden(self):
        elementos = []
        self._en_orden_recursivo(self.raiz, elementos)
        return elementos

    def _en_orden_recursivo(self, nodo, elementos):
        if nodo is not None:
            self._en_orden_recursivo(nodo.izquierda, elementos)
            elementos.append(nodo.valor)
            self._en_orden_recursivo(nodo.derecha, elementos)

def encolar(cantidad_mensajes: int) -> List[str]:
    mensajes = []
    for i in range(cantidad_mensajes):
        response = sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=f"{randint(0, cantidad_mensajes)}",
            MessageGroupId='pagos',
            MessageDeduplicationId=str(uuid.uuid4())
        )
        mensajes.append(response['MessageId'])
        print(f"Enviando Transaction {i} - {response['MessageId']}")
    return mensajes

def desencolar() -> Union[str, None]:
    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=['All'],
        MessageAttributeNames=['All'],
        MaxNumberOfMessages=1,
        VisibilityTimeout=30,
        WaitTimeSeconds=0
    )
    if 'Messages' in response:
        message = response['Messages'][0]
        receipt_handle = message['ReceiptHandle']
        sqs.delete_message(
            QueueUrl=queue_url,
            ReceiptHandle=receipt_handle
        )
        print(f"Mensaje recibido: {message['Body']}")
        return message['Body']
    else:
        print("No se encontraron mensajes en la cola.")
        return None

def generar_arbol(mensajes: List[str]) -> ArbolBinarioBusqueda:
    arbol = ArbolBinarioBusqueda()
    for mensaje in mensajes:
        if mensaje is not None:
            arbol.insertar(int(mensaje))
    return arbol

@app.post("/encolar")
def sqs_encolar(parametros: dict):
    cantidad_mensajes = parametros["cantidad_mensajes"]
    mensajes = encolar(cantidad_mensajes)
    return {
        "Mensajes_encolados": mensajes,
        "cantidad_encolados": len(mensajes)
    }

@app.post("/desencolar")
def sqs_desencolar(parametros: dict):
    cantidad_a_desencolar = parametros.get("cantidad_desencolados", 30)
    mensajes = []
    while len(mensajes) < cantidad_a_desencolar:
        mensaje = desencolar()
        if mensaje is None:
            break
        mensajes.append(mensaje)
    arbol = generar_arbol(mensajes)
    arbol_en_orden = arbol.en_orden()
    return {
        "mensajes_desencolados": mensajes,
        "cantidad_desencolados": len(mensajes),
        "arbol_generado": arbol_en_orden
    }
#AKIA5FTZA63YJJO7YOF7
#tAK3SnetLjOyt9GN91whxfP822uy4tEWyzzXZ/lp